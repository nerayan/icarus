#include <gtest/gtest.h>

#include "icarus/example.h"

namespace {

TEST(ExampleTest, Add) {
  Example ex;

  ASSERT_EQ(ex.Add(2, 3), 5);
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
