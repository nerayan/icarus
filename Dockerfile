FROM alpine:edge

RUN apk add --update --no-cache \
      build-base \
      clang-analyzer \
      cmake \
      doxygen \
      gcovr \
      graphviz \
      gtest \
      gtest-dev \
      ninja
